package com.test;

import com.test.data.AccountDAO;
import com.test.data.InMemoryAccountDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Map;

public class AccountTransferTest {

    private AccountService accountTransfer;

    private AccountDAO accountDAO;

    @Before
    public void setup() {
        accountDAO = new InMemoryAccountDAO(Map.of("A", new Account("A", new BigDecimal(20)), "B", new Account("B", new BigDecimal(8))));
        accountTransfer = new AccountService(accountDAO);
    }

    @Test
    public void testTransfer() {
        boolean isSuccessful = accountTransfer.transfer("A", "B", new BigDecimal(5));
        Assert.assertTrue(isSuccessful);
        Assert.assertEquals(new BigDecimal(15), accountDAO.findById("A").getAmount());
        Assert.assertEquals(new BigDecimal(13), accountDAO.findById("B").getAmount());
    }

    @Test
    public void testCheckLimit() {
        boolean isSuccessful = accountTransfer.transfer("B", "A", new BigDecimal(10));
        Assert.assertFalse(isSuccessful);
        Assert.assertEquals(new BigDecimal(20), accountDAO.findById("A").getAmount());
        Assert.assertEquals(new BigDecimal(8), accountDAO.findById("B").getAmount());
    }


}
