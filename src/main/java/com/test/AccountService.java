package com.test;

import com.test.data.AccountDAO;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class AccountService {

    private Map<String, Lock> lockMap = new ConcurrentHashMap<>();

    private AccountDAO accountDAO;

    protected AccountService(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    public Boolean transfer(String srcAccountId, String destAccountId, BigDecimal amount) {
        Pair<Lock, Lock> lockPair = getLocks(srcAccountId, destAccountId);
        lockPair.getLeft().lock();
        try {
            lockPair.getRight().lock();
            try {
                Account srcAccount = accountDAO.findById(srcAccountId);
                Account destAccount = accountDAO.findById(destAccountId);
                if (srcAccount.getAmount().compareTo(amount) >= 0) {
                    srcAccount.setAmount(srcAccount.getAmount().subtract(amount));
                    destAccount.setAmount(destAccount.getAmount().add(amount));
                    return true;
                }
            } finally {
                lockPair.getRight().unlock();
            }
        } finally {
            lockPair.getLeft().unlock();
        }
        return false;
    }


    public BigDecimal balance(String accountId) {
        return accountDAO.findById(accountId).getAmount();
    }

    private Pair<Lock, Lock> getLocks(String srcAccountId, String destAccountId) {
        Lock lock1;
        Lock lock2;
        if (srcAccountId.compareTo(destAccountId) < 0) {
            lock1 = lockMap.computeIfAbsent(srcAccountId, k -> new ReentrantLock());
            lock2 = lockMap.computeIfAbsent(destAccountId, k -> new ReentrantLock());
        } else {
            lock1 = lockMap.computeIfAbsent(destAccountId, k -> new ReentrantLock());
            lock2 = lockMap.computeIfAbsent(srcAccountId, k -> new ReentrantLock());
        }
        return ImmutablePair.of(lock1, lock2);
    }
}


