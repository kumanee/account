package com.test;

import com.test.data.InMemoryAccountDAO;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

@ApplicationPath("/rest/*")
public class JerseyResourceConfiguration extends ResourceConfig {

    private Map<String, Account> loadData() {
        try (InputStream input = JerseyResourceConfiguration.class.getClassLoader().getResourceAsStream("account.data")) {
            Properties prop = new Properties();
            //load a properties file from class path, inside static method
            prop.load(input);
            Map<String, Account> data = prop.entrySet().stream().collect(Collectors.toMap(e -> (String) e.getKey(), e -> new Account((String) e.getKey(), new BigDecimal((String) e.getValue()))));
            return data;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public JerseyResourceConfiguration() {

        Map<String, Account> data = loadData();
        packages("com.test");
        register(AccountController.class);
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(new AccountService(new InMemoryAccountDAO(data))).to(AccountService.class);
            }
        });
    }
}