package com.test.data;

import com.test.Account;

import java.util.Map;

/**
 * Acts as a repo for the accounts. This is a read only map
 */

public class InMemoryAccountDAO implements AccountDAO {

    private final Map<String, Account> accountMap;


    public InMemoryAccountDAO(Map<String, Account> accountMap) {
        this.accountMap = accountMap;
    }

    public Account findById(String accountId) {
        return accountMap.get(accountId);
    }


}
