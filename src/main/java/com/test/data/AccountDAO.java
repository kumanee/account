package com.test.data;

import com.test.Account;

public interface AccountDAO {

    Account findById(String accountId);

}
