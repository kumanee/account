package com.test;


import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;


@Path("/account")
public class AccountController {

    @Inject
    private AccountService accountService;

    @GET
    @Path("/transfer")
    @Produces(MediaType.TEXT_PLAIN)
    public Boolean transfer(@QueryParam("srcAccount") String srcAccount,
                            @QueryParam("destAccount") String destAccount,
                            @QueryParam("amount") BigDecimal amount) {
        return accountService.transfer(srcAccount, destAccount, amount);
    }

    @GET
    @Path("/balance")
    @Produces(MediaType.TEXT_PLAIN)
    public BigDecimal balance(@QueryParam("account") String account) {
        return accountService.balance(account);
    }


}